//
//  CSNavigationController.swift
//  CSNavigationController
//
//  Created by Chatchawal Saesee on 11/12/15.
//  Copyright © 2015 2C2P. All rights reserved.
//

import UIKit
import Foundation

public class CSNavigationController: UINavigationController {
    
    var color           = UIColor.darkGrayColor()
    var loading         = false
    var nsTimer         :NSTimer!
    var loadingView     :UIActivityIndicatorView!
    var messageView     :UILabel!
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override public init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        self.setLoadingView()
        self.setMessageView()
    }
    
    public init(rootViewController: UIViewController, color: UIColor) {
        super.init(rootViewController: rootViewController)
        self.setThemeColor(color)
    }
    
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setNavigationBarTheme() {
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.translucent = true
        self.navigationBar.backgroundColor = self.color
        self.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    func setStatusBarTheme() {
        let statusBar = UIView()
        statusBar.frame = CGRectMake(0, -20, self.navigationBar.bounds.size.width, 20);
        statusBar.backgroundColor = self.color
        self.navigationBar.addSubview(statusBar)
        self.navigationBar.barStyle = UIBarStyle.Black
    }
    
    func setNavigationTitle() {
        let titleFont = UIFont(name: "Helvetica-Bold", size: 18.0)
        let titleAttr = [NSFontAttributeName: titleFont!, NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationBar.titleTextAttributes = titleAttr
    }
    
    func setBackBarButton() {
        let itemFont = UIFont(name: "Helvetica", size: 0.1)
        let itemAttr = [NSFontAttributeName: itemFont!, NSForegroundColorAttributeName:UIColor(white: 1.0, alpha: 0.8)]
        UIBarButtonItem.appearance().setTitleTextAttributes(itemAttr, forState: UIControlState.Normal)
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), forBarMetrics: UIBarMetrics.Default)
    }
    
    func setLoadingView() {
        let sc = self.navigationBar.bounds
        self.loadingView = UIActivityIndicatorView(frame: CGRectMake((sc.width / 2) - 20, 0, 40, 40))
        self.loadingView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.loadingView.backgroundColor = UIColor.whiteColor()
        self.loadingView.layer.borderColor = self.color.CGColor
        self.loadingView.layer.cornerRadius = 20
        self.loadingView.layer.borderWidth = 2
        self.loadingView.color = self.color
        self.loadingView.startAnimating()
        self.view.insertSubview(self.loadingView, atIndex: 1)
    }
    
    func setMessageView() {
        let sc = self.navigationBar.bounds
        self.messageView = UILabel(frame: CGRectMake(0, 0, sc.width, 0))
        self.messageView.textAlignment = NSTextAlignment.Center
        self.messageView.font = UIFont(name: "Helvetica", size: 16)
        self.messageView.textColor = UIColor.whiteColor()
        self.messageView.lineBreakMode = NSLineBreakMode.ByTruncatingTail
        self.messageView.numberOfLines = 20
        self.view.addSubview(self.messageView)
    }
    
    func showMessageView(text: String, duration: NSTimeInterval, color: UIColor) {
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.Center
        style.firstLineHeadIndent = 10.0
        style.headIndent = 10.0
        style.tailIndent = -10.0
        let attrText = NSAttributedString(string: ("\n\n" + text + "\n"), attributes: [NSParagraphStyleAttributeName:style])
        
        self.messageView.attributedText = attrText
        self.messageView.sizeToFit()
        self.messageView.backgroundColor = color
        self.messageView.frame.origin.y = -self.messageView.frame.size.height
        self.messageView.frame.size.width = self.navigationBar.frame.width
        UIView.animateWithDuration(0.3, animations: {
            var rect = self.messageView.frame
            rect.origin.y = 0
            self.messageView.frame = rect
        })
        
        NSTimer.scheduledTimerWithTimeInterval(duration, target: self, selector: Selector("hideMessageView"), userInfo: nil, repeats: false)
    }
    
    func hideMessageView() {
        UIView.animateWithDuration(0.6, animations: {
            var rect = self.messageView.frame
            rect.origin.y = -self.messageView.frame.height
            self.messageView.frame = rect
        })
    }
    
    public override func setThemeColor(color: UIColor) {
        self.color = color
        self.setNavigationBarTheme()
        self.setStatusBarTheme()
        self.setNavigationTitle()
        self.setLoadingView()
        self.setMessageView()
    }
    
    public override func startLoading() {
        self.loading = true
        self.view.userInteractionEnabled = false
        UIView.animateWithDuration(0.6, animations: {
            var rect = self.loadingView.frame
            rect.origin.y = self.navigationBar.bounds.height + 28
            self.loadingView.frame = rect
            self.view.alpha = 0.8
        })
    }
    
    public override func stopLoading() {
        self.loading = false
        self.view.userInteractionEnabled = true
        
        if (self.nsTimer != nil) {
            self.nsTimer.invalidate()
            self.nsTimer = nil
        }
        
        UIView.animateWithDuration(0.6, animations: {
            var rect = self.loadingView.frame
            rect.origin.y = 0
            self.loadingView.frame = rect
            self.view.alpha = 1.0
        })
    }
    
    public override func isLoading() -> Bool {
        return self.loading
    }
    
    public override func showMessage(text: String, duration: NSTimeInterval) {
        let green = UIColor(red: 107.0/255.0, green: 189/255.0, blue: 57/255.0, alpha: 1.0)
        self.showMessageView(text, duration: duration, color: green)
    }
    
    public override func showWarning(text: String, duration: NSTimeInterval) {
        self.showMessageView(text, duration: duration, color: UIColor.orangeColor())
    }
    
    public override func showError(text: String, duration: NSTimeInterval) {
        self.showMessageView(text, duration: duration, color: UIColor.redColor())
    }
}

public extension UINavigationController {
    
    func setThemeColor(color :UIColor) {
        // abstract method
    }
    
    /** CSNavigationController: Call method after viewDidAppear() for displaying animation on UINavigationBar after ViewDidLoad */
    func startLoading() {
        // abstract method
    }
    
    /** CSNavigationController: Stop UINavigationBar animation after ViewDidLoad */
    func stopLoading() {
        // abstract method
    }
    
    /** CSNavigationController: Checking loading status after ViewDidLoad */
    func isLoading() -> Bool {
        // abstract method
        return false
    }
    
    /** CSNavigationController: Displaying text with green color */
    func showMessage(text: String, duration: NSTimeInterval) {
        // abstract method
    }

    /** CSNavigationController: Displaying text with orange color */
    func showWarning(text: String, duration: NSTimeInterval) {
        // abstract method
    }
    
    /** CSNavigationController: Displaying text with red color */
    func showError(text: String, duration: NSTimeInterval) {
        // abstract method
    }
}













