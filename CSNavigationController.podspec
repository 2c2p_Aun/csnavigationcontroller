Pod::Spec.new do |s|
  s.name = 'CSNavigationController'
  s.version = '1.0.6'
  s.license = 'MIT'
  s.summary = 'My CSNavigationController'
  s.homepage = 'https://bitbucket.org/2c2p_Aun/csnavigationcontroller'
  s.social_media_url = 'http://twitter.com/iamaunz'
  s.authors = { 'Aun Chatchawal' => 'chatchawal@2c2p.com' }
  s.source = { :git => 'https://2c2p_Aun@bitbucket.org/2c2p_Aun/csnavigationcontroller.git', :tag => s.version }

  s.ios.deployment_target = '8.0'
  # s.osx.deployment_target = '10.9'
  # s.tvos.deployment_target = '9.0'
  # s.watchos.deployment_target = '2.0'

  s.frameworks = 'UIKit', 'Foundation'
  s.source_files = 'Source/*.swift'

  s.requires_arc = true
end