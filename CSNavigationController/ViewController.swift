//
//  ViewController.swift
//  CSNavigationController
//
//  Created by Chatchawal Saesee on 11/12/15.
//  Copyright © 2015 2C2P. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.loadScreen()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setThemeColor(randomColor())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadScreen() {
        self.title = "Title"
        self.view.backgroundColor = UIColor.whiteColor()
    
        let btnNext = UIButton(frame: CGRectMake(20, 150, 280, 44))
        btnNext.addTarget(self, action: Selector("nextButtonPressed"), forControlEvents: UIControlEvents.TouchUpInside)
        btnNext.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        btnNext.setTitleColor(UIColor.greenColor(), forState: UIControlState.Highlighted)
        btnNext.setTitle("Next", forState: UIControlState.Normal)
        btnNext.backgroundColor = UIColor.grayColor()
        self.view.addSubview(btnNext)
        
        let btnShow = UIButton(frame: CGRectMake(20, 250, 280, 44))
        btnShow.addTarget(self, action: Selector("showButtonPressed"), forControlEvents: UIControlEvents.TouchUpInside)
        btnShow.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        btnShow.setTitleColor(UIColor.greenColor(), forState: UIControlState.Highlighted)
        btnShow.setTitle("Show", forState: UIControlState.Normal)
        btnShow.backgroundColor = UIColor.grayColor()
        self.view.addSubview(btnShow)
        
        let btnMessage = UIButton(frame: CGRectMake(20, 350, 280, 44))
        btnMessage.addTarget(self, action: Selector("messageButtonPressed"), forControlEvents: UIControlEvents.TouchUpInside)
        btnMessage.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        btnMessage.setTitleColor(UIColor.greenColor(), forState: UIControlState.Highlighted)
        btnMessage.setTitle("Message", forState: UIControlState.Normal)
        btnMessage.backgroundColor = UIColor.grayColor()
        self.view.addSubview(btnMessage)
    }
    
    func nextButtonPressed() {
        let viewController = ViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showButtonPressed() {
        if (self.navigationController?.isLoading() == true) {
            self.navigationController?.stopLoading()
        }else{
            self.navigationController?.startLoading()
            NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("stop"), userInfo: nil, repeats: false)
        }
    }
    
    func messageButtonPressed() {
        self.navigationController?.showMessage("Welcome to the Losum Family page at Surname Finder, a service of Genealogy Today. Our editors have compiled.", duration: 5.0)
    }
    
    func stop() {
        self.navigationController?.stopLoading()
    }
    
    func randomColor() -> UIColor {
        let r = CGFloat(arc4random()) / CGFloat(UInt32.max)
        let g = CGFloat(arc4random()) / CGFloat(UInt32.max)
        let b = CGFloat(arc4random()) / CGFloat(UInt32.max)
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
}







